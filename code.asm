section	.data
		base64  db  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	
section	.text
		global code					;void code(char* input, char* output)


code:
	push	ebp
	mov		ebp, esp

	;push 	edi
	push 	ebx
	push 	edx
	push 	ecx
	
	xor 	eax, eax
	xor 	ebx, ebx
	xor 	ecx, ecx
	xor 	edx, edx
	
	mov 	esi, dword[ebp+8] 		;load input address
	mov 	edi, dword[ebp+12]		;load output address
	mov 	ebx, base64 			;load base64 table address
	
code_loop:

	;load 3 bytes into eax
	mov 	ah, byte[esi]
	cmp 	ah, 0x0
	je 		exit 					;jump to exit if end of input
	inc 	esi

	mov 	al, byte[esi]
	cmp 	al, 0x0
	je 		one_byte 				;only one character, padding
	inc 	esi

	shl 	eax, 8
	mov 	al, byte[esi]
	cmp 	al, 0x0
	je 		two_bytes 				;two characters, padding
	inc 	esi


	;first 6 bits
	mov 	edx, eax	
	shr 	edx, 18
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;second 6 bits
	mov 	edx, eax
	shr 	edx, 12
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;third 6 bits
	mov 	edx, eax
	shr 	edx, 6
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi
	
	;last 6 bits
	mov 	edx, eax
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	jmp 	code_loop 				;code_loop


one_byte:
	;mov 	al, 0					;only 2 bytes in register

	;first 6 bits
	mov 	edx, eax
	shr 	edx, 10
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;second 6 bits
	mov 	edx, eax
	shr 	edx, 4
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;padding
	mov 	byte[edi], '='
	inc 	edi
	mov 	byte[edi], '='

	jmp 	exit

two_bytes:
	;mov 	al, 0					;only 3 bytes in register, last is 0

	;first 6 bits
	mov 	edx, eax
	shr 	edx, 18
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;second 6 bits
	mov 	edx, eax
	shr 	edx, 12
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi

	;third 6 bits
	mov 	edx, eax
	shr 	edx, 6
	and 	edx, 0x3F
	add 	edx, ebx
	mov 	dl, byte[edx]
	mov 	byte[edi], dl
	inc 	edi
	
	;padding
	mov 	byte[edi], '='
	
exit:
	mov 	eax, 0

	pop 	ecx
	pop 	edx
	pop 	ebx
	;pop 	edi
	
	mov 	esp, ebp
	pop 	ebp

	ret