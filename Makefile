CC=gcc
CFLAGS=-m32 -g

ASM=nasm
AFLAGS=-f elf32 -g

all: 		base64

main.o: 	main.c
			$(CC) $(CFLAGS) -c main.c

code.o: 	code.asm
			$(ASM) $(AFLAGS) code.asm

decode.o:	decode.asm
			$(ASM) $(AFLAGS) decode.asm

base64: 	main.o code.o decode.o 
			$(CC) $(CFLAGS) main.o code.o decode.o -o base64
clean:
			rm *.o
			rm base64
