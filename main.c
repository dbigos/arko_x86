#include <stdio.h>
#include <string.h>

#ifdef _cplusplus
extern "C" {
#endif
  void code(char *input, char *output);
  void decode(char *input, char *output);
#ifdef _cplusplus
}
#endif

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("Argument needed!\n");
		return -1;
	}

	FILE *fin = NULL, *fout = NULL;

	int i;
	char name_in[16], name_out[16];

	printf("Input file:\n");
	scanf("%s", name_in);
	printf("Output file:\n");
	scanf("%s", name_out);

	fin = fopen(name_in, "r");
	fout = fopen(name_out, "w");
	
	if(!fin)
	{
		printf("Failed to open input file\n");
		fclose(fin);
		return -1;
	}

	if(!fout)
	{
		printf("Failed to open output file\n");
		fclose(fout);
		fclose(fin);
	}
	
	if(atoi(argv[1]) == 1)
	{
		char input[12001];
		char output[16000];

		for(i = 0; i < 12001; ++i)
			input[i] = 0x0;

		for(i = 0; i < 16000; ++i)
			output[i] = 0x0;

  		while(fread(input, 12000, 1, fin) == 12000)
  		{
  			code(input, output);
  			fwrite(output, strlen(output), 1, fout);

  			for(i = 0; i < 12001; ++i)
				input[i] = 0x0;

			for(i = 0; i < 16000; ++i)
				output[i] = 0x0;
 		}

 		code(input,output);
 		fwrite(output, strlen(output), 1, fout);
  		
  		printf("Coding ended successfully\n");
 	}

 	if(atoi(argv[1]) == 2)
	{
		char input[16001];
		char output[12000];

		for(i = 0; i < 16001; ++i)
				input[i] = 0x0;

		for(i = 0; i < 12000; ++i)
			output[i] = 0x0;

  		while(fread(input, 16000, 1, fin) == 16000)
  		{
  			decode(input, output);
  			fwrite(output, strlen(output), 1, fout);
  			
  			for(i = 0; i < 16001; ++i)
				input[i] = 0x0;

			for(i = 0; i < 12000; ++i)
				output[i] = 0x0;
 		}
 		
 		decode(input, output);
  		fwrite(output, strlen(output), 1, fout);

 		printf("Decoding ended successfully\n");
 	}


  	fclose(fin);
  	fclose(fout);

  	return 0;
}

