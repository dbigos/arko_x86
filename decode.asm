section	.data
		base64  db  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	
section	.text
		global decode				;void decode(char* input, char* output)


decode:
	push	ebp
	mov		ebp, esp

	push 	ebx
	push 	edx
	push 	ecx
	
	xor 	eax, eax
	xor 	ebx, ebx
	xor 	ecx, ecx
	xor 	edx, edx
	
	mov 	esi, dword[ebp+8] 		;load input address
	mov 	edi, dword[ebp+12]		;load output address
	mov 	ebx, base64 			;load base64 table address
	
decode_loop:
	mov 	cl, byte[esi] 			;first character
	inc 	esi
	cmp 	cl, 0x0
	je 		exit

	mov 	edx, ebx

find1:
	mov 	ch, byte[edx]
	inc 	edx
	cmp 	ch, cl
	jne 	find1

	dec 	edx
	sub 	edx, ebx
	mov 	eax, edx
	shl 	eax, 6

	mov 	cl, byte[esi]			;second character
	inc 	esi
	mov 	edx, ebx

find2:
	mov 	ch, byte[edx]
	inc 	edx
	cmp 	ch, cl
	jne 	find2

	dec 	edx
	sub 	edx, ebx
	add 	eax, edx
	shl 	eax, 6

	mov 	cl, byte[esi]			;third character
	inc 	esi
	cmp 	cl, "="
	je 		one_byte
	mov 	edx, ebx

find3:
	mov 	ch, byte[edx]
	inc 	edx
	cmp 	ch, cl
	jne 	find3

	dec 	edx
	sub 	edx, ebx
	add 	eax, edx
	shl 	eax, 6

	mov 	cl, byte[esi] 			;fourth character
	inc 	esi
	cmp 	cl, "="
	je 		two_bytes
	mov 	edx, ebx

find4:
	mov 	ch, byte[edx]
	inc 	edx
	cmp 	ch, cl
	jne 	find4

	dec 	edx
	sub 	edx, ebx
	add 	eax, edx

	;first byte
	mov 	edx, eax
	shr 	edx, 16
	and 	edx, 0xFF
	mov 	byte[edi], dl
	inc 	edi

	;second byte
	mov 	edx, eax
	shr 	edx, 8
	and 	edx, 0xFF
	mov 	byte[edi], dl
	inc 	edi

	;third 	byte
	and 	eax, 0xFF
	mov 	byte[edi], al
	inc 	edi

	jmp 	decode_loop

one_byte:
	shr 	eax, 10
	and 	eax, 0xFF
	mov 	byte[edi], al
	jmp 	exit
	
two_bytes:
	mov 	edx, eax
	shr 	edx, 16
	and 	edx, 0xFF
	mov 	byte[edi], dl
	inc 	edi

	shr 	eax, 8
	and 	eax, 0xFF
	mov 	byte[edi], al


exit:
	pop 	ecx
	pop 	edx
	pop 	ebx
	
	mov 	esp, ebp
	pop 	ebp

	ret